<?php
	// エラーメッセージの初期化
	$errorMessage = "";

	// TODO：認証処理
	// ログインボタンが押された場合
	if (isset($_POST["password"])) {

	    if(($_POST['password']) === "testPassword" && ($_POST['user-name']) === "testUser"){
	        header("Location: index.php");  // メイン画面へ遷移
	    }else if(empty($_POST['password'])){
	        $errorMessage = 'パスワードが未入力です。';
	    }else{
	        $errorMessage = 'パスワードに誤りがあります。';
	    }
	}

?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>FileUpload</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard_copy.css" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

</head>
<body>

<div class="wrapper">
    <div class="main-panel">


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                <div class="row">

				        <div class="row"><br><br><br><br><br><br>
				          <div class="login-header">
				            <h3 class="text-center">ログイン</h3>
				          </div>   
				        </div>   
						<form name="form1" action="" method="post">
			              <div class="well col-md-10 col-md-offset-1 login-box">
			                <div class="input-group input-group-lg col-md-10 col-md-offset-1">
			                  <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
			                  <input type="text" name="user-name" class="form-control" placeholder="ユーザ名">
			                </div>   
			                <div class="input-group input-group-lg col-md-10 col-md-offset-1">
			                  <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
			                  <input type="password" name="password" class="form-control" placeholder="パスワード">
			                </div>   
			                <button type="submit" name="login" class="btn btn-default center-block">Login</button>
			                <div class="center-block"><font color="#ff0000"><?php echo htmlspecialchars($errorMessage, ENT_QUOTES); ?></font>
			                </div>
			              </div>   
			            </form>
                </div>
                </div>
            </div>
        </div>



    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="assets/js/paper-dashboard.js"></script>

	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>
