<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>FileUpload</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

</head>
<body>

<div class="wrapper">
	<div class="sidebar" data-background-color="white" data-active-color="danger">
    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="index.php" class="simple-text">
                    HOME
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="index.php">
                        <i class="ti-panel"></i>
                        <p>ファイル</p>
                    </a>
                </li>
                <li>
                    <a href="user.php">
                        <i class="ti-user"></i>
                        <p>ユーザ</p>
                    </a>
                </li>
                <li>
                    <a href="table.php">
                        <i class="ti-view-list-alt"></i>
                        <p>テーブル</p>
                    </a>
                </li>
                <li>
                    <a href="icons.php">
                        <i class="ti-pencil-alt2"></i>
                        <p>アイコン</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">ファイル一覧</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                    </ul>

                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>

                                <form method="post" action="upload.php" enctype="multipart/form-data">
                                    <div class="text-right">
<!--                                         <input type="file" name="up_file" class="btn btn-fill btn-wd"/>
 -->
                                        <label for="InputFile" class="btn btn-default">ファイル選択
                                            <input class="hide" type="file" name="up_file" id="InputFile" onchange="$('#file_name').text($(this).val())"/>
                                        </label>
                                        <span id="file_name"></span>                                     
                                        <button type="submit" class="btn btn-fill btn-wd ">ファイルアップロード
                                        </button>
                                    </div>
                                </form>
                            </div>

                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped">
                                    <thead>
                                        <th>No</th>
                                    	<th>ファイル名</th>
                                    	<th>更新日時</th>
                                    </thead>
                                    <tbody>
                                    <?php
                                        // No、ファイル名、更新日時を、テーブルに表示する
                                        // TODO:ファイル情報をDBで保持する
                                        $count = 1;
                                        $path = "upload-files/"; // ファイルの保存場所
                                        $length = strlen($path) ; // ファイルパスのlength

                                        // globで upload-files配下のファイル情報を全て取得
                                        foreach(glob($path.'*.*')  as $file){
                                            if(is_file($file)){
                                                // substr で $file のディレクトリ名をカット
                                                $filename = substr($file,$length);

                                                // ファイルのフルパス取得して、ファイルの最終更新日を取得
                                                $fullPath = $path. $filename;
                                                $updateDate = date("Y/m/d H:i", filemtime($fullPath));

                                                // テーブルに出力
                                                echo "<tr><td>".$count."</td><td><a href='file.php?name=".$filename."'>".$filename."</td></a><td>".$updateDate."</td></tr>";
                                            }
                                            $count++;
                                        }

                                        // // 元ページへ遷移
                                        // $uri = $_SERVER['HTTP_REFERER'];
                                        // header("Location: ".$uri);

                                    ?>                                        
                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="assets/js/paper-dashboard.js"></script>

	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>


</html>
