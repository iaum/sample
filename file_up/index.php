<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>FileUpload</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

</head>
<body>
<div class="wrapper">
	<div class="sidebar" data-background-color="white" data-active-color="danger">
    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="index.php" class="simple-text">
                    <h5 class="text-left">HOME</h5>
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="">
                        <i class="ti-panel"></i>
                        <p>ファイル</p>
                    </a>
                </li>
                <li>
                    <a href="user.php">
                        <i class="ti-user"></i>
                        <p>ユーザ</p>
                    </a>
                </li>
                <li>
                    <a href="table.php">
                        <i class="ti-view-list-alt"></i>
                        <p>テーブル</p>
                    </a>
                </li>
                <li>
                    <a href="icons.php">
                        <i class="ti-pencil-alt2"></i>
                        <p>アイコン</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">ファイル</a>
                </div>
                <div class="collapse navbar-collapse">
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="places-buttons">
                                <div class="content">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <h4>ソース　サンプル集
                                                <p class="category"></p>
                                            </h4>
                                        </div>
                                        <div class="row col-md-10 col-md-offset-1">
                                            <div class="col-md-4">
                                                <a href="fileList.php">
                                                    <button name="jquery" class="btn btn-default btn-block" onclick="demo.showNotification('top','left')">Spring Boot</button>
                                                </a>
                                            </div>
                                            <div class="col-md-4">
                                                <a href="fileList.php">
                                                    <button class="btn btn-default btn-block" onclick="demo.showNotification('top','center')">Asp.net</button>
                                                </a>
                                            </div>
                                            <div class="col-md-4">
                                                <a href="fileList.php">
                                                    <button class="btn btn-default btn-block" onclick="demo.showNotification('top','right')">Java</button>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row col-md-10 col-md-offset-1">
                                            <div class="col-md-4">
                                                <a href="fileList.php">
                                                    <button class="btn btn-default btn-block" onclick="demo.showNotification('bottom','left')">JQuery</button>
                                                </a>
                                            </div>
                                            <div class="col-md-4">
                                                <a href="fileList.php">
                                                    <button class="btn btn-default btn-block" onclick="demo.showNotification('bottom','center')">Style Sheet</button>
                                                </a>
                                            </div>
                                            <div class="col-md-4">
                                                <a href="fileList.php">
                                                    <button class="btn btn-default btn-block" onclick="demo.showNotification('bottom','right')">PHP</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="assets/js/paper-dashboard.js"></script>

	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>


</html>
